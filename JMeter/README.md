# Running the application

Procede to the bin/ folder, and start the application using an appropriate executable for your operating system. 

On linux a use command:

./jmeter


# Test Scenario

The test scenario is available in bin/templates/FYP.jmx
