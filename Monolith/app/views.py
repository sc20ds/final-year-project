from functools import wraps

from app import db

from flask.helpers import url_for
from flask import Response, jsonify
import requests

from app import app
from flask import render_template, flash, redirect, request, session, abort

from app.models import Product, ProductCategory, User

from .forms import AddItemForm, CategoryAdditionForm, EditItemForm, EditUserForm, LoginForm, ProductSearchForm, ProductSearchFormSeller, RegisterForm, SellerRegisterForm
from datetime import date, datetime, timedelta, timezone

import os
from werkzeug.utils import secure_filename

import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry

import json
from time import time, sleep

graphs = {}
graphs['success'] = Counter(
    'monolith_successful_requests_total', 'Number of successful requests.')
graphs['error'] = Counter('monolith_error_requests_total',
                          'Number of error requests.')

graphs['c'] = Counter('monolith_request_operations_total',
                      'Total number of requests')
graphs['h'] = Histogram('monolith_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))

# decorator function to make sure user is logged in to access certain pages


def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function


def login_required(f):
    @wraps(f)
    def login_required_decorated_function(*args, **kwargs):
        if not (session.get("loggedIn", default=False) == True and
                datetime.utcnow().replace(tzinfo=timezone.utc) - session.get("lastSeen", default=datetime(year=1, month=1, day=1, tzinfo=timezone.utc)) < timedelta(minutes=app.logged_in_time)):
            return redirect('/login')
        return f(*args, **kwargs)
    return login_required_decorated_function


def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try:
            if response.status_code == 200:
                graphs['success'].inc()
            else:
                graphs['error'].inc()
        except:
            graphs['success'].inc()
        return response
    return wrapper


@app.route('/tester', methods=['GET', 'POST'])
@time_request
@count_requests
@categorize_response
def tester():
    start_time = time()
    graphs['c'].inc()
    sleep(0.6)
    end_time = time()
    graphs['h'].observe(end_time - start_time)
    return Response("I am just a tester", status=200, content_type='text/html')


@app.route('/metrics')
def metrics():
    res = []
    for k, v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')

# login page


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
@time_request
@count_requests
@categorize_response
def login():
    # if logged in, redirect to home
    if session.get("loggedIn", default=False) == True and \
            datetime.utcnow().replace(tzinfo=timezone.utc) - session.get("lastSeen", default=datetime(year=1, month=1, day=1, tzinfo=timezone.utc)) < timedelta(minutes=app.logged_in_time):
        return redirect('/home')
    # Otherwise log in
    form = LoginForm()

    if form.validate_on_submit():
        # Check if the user exists
        username = form.username.data
        password = form.password.data

        data = {
            "username": username,
            "password": password
        }


        # if user exists, log in
        if auth_login(data) == 200:
            session["userName"] = username
            session["loggedIn"] = True
            session["lastSeen"] = datetime.utcnow()

            return redirect('/home')
        else:  # otherwise render failed login
            return Response(render_template('failed_login.html', message="Login Failed"), status=200, content_type='text/html')

    return Response(render_template('login.html', form=form), status=200, content_type='text/html')


@app.route('/register', methods=['POST', 'GET'])
@time_request
@count_requests
@categorize_response
def register():
    form = RegisterForm()
    if form.validate_on_submit():

        data = {
            "username": form.username.data,
            "password": form.password.data,
            "firstname": form.firstname.data,
            "surname": form.surname.data
        }
        auth_register(data)

        return redirect('/login')

    return Response(render_template('register.html',
                                    title='Register',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/logout', methods=['POST', 'GET'])
@time_request
@count_requests
@categorize_response
def logout():
    session["loggedIn"] = False
    session["userName"] = ""
    session["lastSeen"] = 123

    return redirect('/login')


@app.route('/home', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def home():
    return Response(render_template('home.html', username=session['userName']),
                    status=200,
                    content_type='text/html')


@app.route('/all_items', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def all_items():
    items = storage_all_items()

    return Response(render_template('all_items.html', items=items),
                    status=200,
                    content_type='text/html')


@app.route('/delete_item', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def delete_item():

    my_id = auth_get_my_id(session.get('userName'))

    item = storage_get_item(request.form["item_id"])
    if item == None:
        return redirect("/home")

    storage_delete(request.form['item_id'])
    return redirect('/home')


@app.route('/delete_from_basket/<removed_id>', methods=['GET', 'POST'])
@app.route('/delete_from_basket', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def delete_from_basket(removed_id=None):
    if removed_id == None:
        if 'item_id' not in request.form.keys():
            abort(400, "Delete from basket had no item_id param")

        if request.form.get('item_id') in app.basket[session.get('userName')]:
            app.basket[session.get('userName')].remove(
                request.form.get('item_id'))

        return redirect('/basket')
    else:

        if removed_id in app.basket[session.get('userName')]:
            app.basket[session.get('userName')].remove(removed_id)

        return redirect('/basket')


@app.route('/add_to_basket', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def add_to_basket():
    print("request.form:", request.form)
    if 'item_id' not in request.form.keys():
        abort(400, "Bad Request")

    if session.get('userName', default=None) == None:
        abort(401, "Unauthorized")
    if session.get('userName') not in app.basket.keys():
        app.basket[session.get('userName')] = set()

    app.basket[session.get('userName')].add(request.form.get('item_id'))

    return redirect('/basket')


@app.route('/basket', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def basket():

    if session.get('userName') not in app.basket.keys():
        app.basket[session.get('userName')] = set()

    basket_items = []
    for item in app.basket[session.get('userName')]:
        response = storage_get_item(item)
        if response != None:
            basket_items.append(response)

    return Response(render_template('basket.html', basket_items=basket_items),
                    status=200,
                    content_type='text/html')


@app.route('/edit_item', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def edit_item():
    form = EditItemForm()
    if form.validate_on_submit():

        data = {}
        if form.price.data and form.price.data != "":
            data['price'] = form.price.data
        if form.in_stock.data and form.in_stock.data != "":
            data['in_stock'] = form.in_stock.data
        if form.product_name.data and form.product_name.data != "":
            data['product_name'] = form.product_name.data
        if form.description.data and form.description.data != "":
            data['description'] = form.description.data

        storage_put(form.item_id.data)
        return redirect('/home')

    if 'item_id' in request.args.keys():
        form.set_hidden_value(request.args['item_id'])

    return Response(render_template('edit_item.html',
                                    title='AddItem',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/add_item', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def add_item():

    form = AddItemForm()
    if form.validate_on_submit():
        my_id = auth_get_my_id(session.get('userName'))

        data = {
            "price": form.price.data,
            "in_stock": form.in_stock.data,
            "product_name": form.product_name.data,
            "description": form.description.data,
            "selling_user_id": my_id
        }
        storage_post(data)
        return redirect('/home')

    return Response(render_template('add_item.html',
                                    title='AddItem',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/my_items', methods=['POST', 'GET'])
@login_required
@time_request
@count_requests
@categorize_response
def my_items():
    my_id = auth_get_my_id(session.get('userName'))

    items = search_get_by_seller(my_id)

    return Response(render_template(
        'your_found_items.html',
        seller_items=items),
        status=200,
        content_type='text/html')


@app.route('/item_search', methods=['POST', 'GET'])
@login_required
@time_request
@count_requests
@categorize_response
def item_search():

    form = ProductSearchForm()
    if form.validate_on_submit():

        found_items = []
        unique_ids = set()

        if form.product_name.data != "":
            items = search_get_all_by_name(form.product_name.data)
            
            for item in items:
                if item.product_id not in unique_ids:
                    unique_ids.add(item.product_id)
                    found_items.append(item)

        if form.seller.data and form.seller.data != "":
            items = search_get_by_seller(form.seller.data)
            for item in items:
                if item.product_id not in unique_ids:
                    unique_ids.add(item.product_id)
                    found_items.append(item)

        if form.price_min.data and form.price_min.data != "":
            found_items = list(
                filter(lambda x: x['price'] >= form.price_min.data, found_items))
        if form.price_min.data and form.price_max.data != "":
            found_items = list(
                filter(lambda x: x['price'] <= form.price_max.data, found_items))
        if form.price_min.data and form.in_stock.data != "":
            found_items = list(
                filter(lambda x: x['in_stock'] > 0, found_items))

        return Response(render_template(
            'found_items.html',
            found_items=list(found_items)),
            status=200,
            content_type='text/html')

    return Response(render_template('item_search.html',
                                    title='Search Items',
                                    form=form),
                    status=200,
                    content_type='text/html')

@app.route('/user_settings', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def user_settings():

    message = ""

    form = EditUserForm()
    if form.validate_on_submit():
        data = {
            "current_username": session.get('userName'),
            "current_password": form.current_password.data,
            "password": form.password.data
        }
        auth_update_user(data)

    return Response(render_template('user_settings.html',
                                    form=form,
                                    message=message),
                    status=200,
                    content_type='text/html')


@app.route('/checkout', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def checkout():

    total = 0
    for product, value in request.form.items():
        if product[:8] != 'product_':
            continue
        product_id = product[8:]
        db_item = storage_get_item(product_id)
        total += db_item.price * int(value)

    products = [[k, v] for k, v in request.form.items()]

    return Response(render_template('show_total.html',
                                    total=total,
                                    products=products),
                    status=200,
                    content_type='text/html')


@time_request
@count_requests
@categorize_response
def payment_pay(data):
    try:
        card_number = data.get('card_number')
        cvv = data.get('cvv')
        expiry_date = data.get('expiry_date')
    except:
        return 400

    if card_number == "":
        return 400
    if cvv == "":
        return 400
    if expiry_date == "":
        return 400

    if card_number == '4242' * 4:
        return 200
    else:
        return 402


@app.route('/pay', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def pay():
    itemlist = json.loads(request.form.get('prods').replace("'", '"'))

    data = {
        "card_number": request.form.get("card_number"),
        "cvv": request.form.get("cvv"),
        "expiry_date": request.form.get("expiry_date")
    }

    resp_code = payment_pay(data)

    if resp_code == 200:
        app.basket[session.get('userName')] = set()
        return Response(render_template('paymentSuccess.html'),
                        status=200,
                        content_type='text/html')
    else:
        return Response(render_template('paymentFailed.html'),
                        status=200,
                        content_type='text/html')



@time_request
@count_requests
@categorize_response
def search_get_all_by_name(name):

    items = storage_all_items()

    return [ product for product in items if product.product_name == name ]

@time_request
@count_requests
@categorize_response
def search_get_by_id(id):
    return storage_get_item(id)

@time_request
@count_requests
@categorize_response
def search_get_by_seller(selling_user_id):
    items = storage_all_items()
    return [ product for product in items if product.selling_user_id == int(selling_user_id) ]


@time_request
@count_requests
@categorize_response
def storage_all_items():
    items_from_db = Product.query.all()
    return [ item for item in items_from_db ]


@time_request
@count_requests
@categorize_response
def storage_get_item(item_id):
    item_from_db = Product.query.get(item_id)
    if item_from_db == None:
        return None
    return item_from_db


@time_request
@count_requests
@categorize_response
def storage_put(item_id, data):
    product = Product.query.get(item_id)
    if product == None:
        return

    if data.get('username') != None:
        product.price = data['price']

    if data.get('in_stock') != None:
        if isinstance(data.get('in_stock'), int):
            product.in_stock = data['in_stock']

    if data.get('product_name') != None and isinstance(data.get('product_name'), str):
        product.product_name = data['product_name']

    if data.get('description') != None:
        product.description = data['description']

    db.session.commit()

    if 'categories' in data:
        product.categories = []
        for cat in json.loads(data['categories']):
            category = ProductCategory.query.get(cat)
            if category != None:
                product.categories.append(category)

    db.session.commit()


@time_request
@count_requests
@categorize_response
def storage_delete(item_id):
    product = Product.query.get(item_id)
    if product == None:
        response = {'message': "Product with this ID doesn't exist"}
        return response, 401

    Product.query.filter_by(product_id=item_id).delete()
    db.session.commit()
    return {"message:": "Removed"}, 200


@time_request
@count_requests
@categorize_response
def storage_post(data):

    if data.get('price') == None:
        return
    if data.get('in_stock') == None or not isinstance(data['in_stock'], int):
        return
    if data.get('product_name') == None:
        return
    if data.get('description') == None:
        return
    if data.get('selling_user_id') == None:
        return

    new_product = Product(
        price = data.get('price'),
        in_stock = data.get('in_stock'),
        product_name = data.get('product_name'),
        description = data.get('description'),
        selling_user_id = data.get('selling_user_id'),
    )

    db.session.add(new_product)
    db.session.commit()

    if 'categories' in data:
        for cat in json.loads(data.get('categories')):
            category = ProductCategory.query.get(cat)
            if category != None:
                new_product.categories.append(category)
    db.session.commit()
    



@time_request
@count_requests
@categorize_response
def auth_register(data):

    try:
        username = data.get('username')
        password = data.get('password')
        firstname = data.get('firstname')
        surname = data.get('surname')
    except Exception as e:
        return

    if username == "":
        return 
    if password == "":
        return 
    if firstname == "":
        return 
    if surname == "":
        return 

    if User.query.filter_by(username = username).first():
        return 

    new_user = User(
        username = username,
        password = password,
        firstname = firstname,
        surname = surname,
        updated_at = datetime.utcnow(),
        created_at = datetime.utcnow()
    )
    db.session.add(new_user)
    db.session.commit()

@time_request
@count_requests
@categorize_response
def auth_login(data):
    try:
        username = data.get('username')
        password = data.get('password')
    except:
        return
                    
    if username == "":
        return
    if password == "":
        return 
    
    if User.query.filter_by(username = username, password = password).first():
        return 200

    return  401

@time_request
@count_requests
@categorize_response
def auth_get_my_id(username):

    if username == "":
        return None

    user = User.query.filter_by(username = username).first()
    if not user:
        None
    return user.user_id 


@time_request
@count_requests
@categorize_response
def auth_update_user(data):

    current_username = data.get('current_username')
    current_password = data.get('current_password')

    new_username = data.get('username')
    new_password = data.get('password')
    new_firstname = data.get('firstname')
    new_lastname = data.get('lastname')

    # Connect to the database and retrieve the user with the given username
    user = User.query.filter_by(username = current_username, password = current_password).first()
    if not user:
        return None

    # Update the username, password, firstname, and/or lastname if they were provided
    if new_username:
        user.username = new_username
    if new_password:
        user.password = new_password
    if new_firstname:
        user.firstname = new_firstname
    if new_lastname:
        user.lastname = new_lastname

    # Save the updated user to the database
    db.session.commit()
