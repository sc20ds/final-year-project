from flask import Flask
import os
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from werkzeug import test
app = Flask(__name__)

app.config.from_object('config')

UPLOAD_FOLDER = os.path.join('app', 'static', 'uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.logged_in_time = 5 * 60 
app.basket = dict()
app.users_logged_in = dict()

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app.models import Product

db.create_all()


from app import views


