kubectl apply -f auth-db-config.yaml
kubectl apply -f auth-db-secret.yaml
kubectl apply -f auth-db.yaml

kubectl apply -f auth.yaml

kubectl apply -f storage-db-config.yaml
kubectl apply -f storage-db-secret.yaml
kubectl apply -f storage-db.yaml

kubectl apply -f storage.yaml

kubectl apply -f search-config.yaml
kubectl apply -f search.yaml

kubectl apply -f payment.yaml

kubectl apply -f front-config.yaml
kubectl apply -f front.yaml

kubectl apply -f monolith.yaml
kubectl apply -f monolith-db-config.yaml
kubectl apply -f monolith-db-secret.yaml
kubectl apply -f monolith-db.yaml

helm install prometheus prometheus-community/prometheus -f prometheus-values.yaml
helm install grafana grafana/grafana
