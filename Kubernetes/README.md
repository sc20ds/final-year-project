#### deploy using:
bash deploy.sh


#### Remove using:
bash destroy.sh

#### Grafana Password
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
