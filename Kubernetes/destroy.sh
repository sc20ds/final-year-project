kubectl delete -f auth-db-config.yaml
kubectl delete -f auth-db-secret.yaml
kubectl delete -f auth-db.yaml

kubectl delete -f auth.yaml

kubectl delete -f storage-db-config.yaml
kubectl delete -f storage-db-secret.yaml
kubectl delete -f storage-db.yaml

kubectl delete -f storage.yaml

kubectl delete -f search-config.yaml
kubectl delete -f search.yaml

kubectl delete -f payment.yaml

kubectl delete -f front-config.yaml
kubectl delete -f front.yaml

kubectl delete -f monolith.yaml
kubectl delete -f monolith-db-config.yaml
kubectl delete -f monolith-db-secret.yaml
kubectl delete -f monolith-db.yaml

helm uninstall prometheus
helm uninstall grafana
