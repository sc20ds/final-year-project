import os

# Enabling CSRF protection
WTF_CSRF_ENABLED = False
SECRET_KEY = 'secret-key-1234-#$%^'

# setting prject's base directory
basedir = os.path.abspath(os.path.dirname(__file__))

PAYMENT_SERVICE_URL = os.environ.get("PAYMENT_SERVICE_URL")
SEARCH_SERVICE_URL = os.environ.get("SEARCH_SERVICE_URL")
STORAGE_SERVICE_URL = os.environ.get("STORAGE_SERVICE_URL")
AUTH_SERVICE_URL = os.environ.get("AUTH_SERVICE_URL")

# PAYMENT_SERVICE_URL = "http://localhost:4998"
# SEARCH_SERVICE_URL = "http://localhost:4997"
# STORAGE_SERVICE_URL = "http://localhost:4996"
# AUTH_SERVICE_URL = "http://localhost:4999"
