from flask.globals import session
from flask_wtf import FlaskForm
from wtforms import BooleanField, DecimalField, HiddenField, IntegerField, StringField
from wtforms.fields.simple import PasswordField, SubmitField
from wtforms.validators import DataRequired, ValidationError

from wtforms import validators
import re


class ProductSearchFormSeller(FlaskForm):
    product_name = StringField('product_name')
    category = StringField('category')
    
    # Filter by how many are in stock (useful for finding empty entries)
    in_stock = IntegerField('in_stock', validators=[validators.Optional()])
    submit = SubmitField('Search')
    
    def validate(self, extra_validators=None):
        if super().validate(extra_validators):
            if not (self.product_name.data or self.category.data or self.in_stock.data):
                self.product_name.errors.append(
                    'Choose a product name, category or the amount in stock')
                return False
            else:
                return True

        return False

class ProductSearchForm(FlaskForm):
    product_name = StringField('product_name')
    seller = StringField('seller')
    
    # Filter if there are any in stock
    in_stock = BooleanField('in_stock')

    price_min = DecimalField('price_min', validators=[validators.Optional()])
    price_max = DecimalField('price_max', validators=[validators.Optional()])

    def validate(self, extra_validators=None):
        if super().validate(extra_validators):
            if not (self.product_name.data or self.seller.data):
                self.product_name.errors.append(
                    'Choose a product name, category or the seller')
                return False
            else:
                return True
        return False
    submit = SubmitField('search')

    

def password_check(form, field):
    password = form.password.data
    if re.search('[0-9]', password) is None:
        raise ValidationError('Password must contain a number')
    elif re.search('[A-Z]', password) is None:
        raise ValidationError('Password must have one uppercase letter')

def username_check(form, field):
    if True:
        'a'
    # username = form.username.data
    # if User.query.filter_by(username=username).first() is not None or \
    #     Seller.query.filter_by(username=username).first() is not None:
    #     raise ValidationError('User with this username already exsits')


class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[
        DataRequired(),
        ])
    password = PasswordField('Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=40, message = "password between 8 and 40 characters"),
        validators.EqualTo('confirm', message = 'Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')    
    firstname = StringField('First Name', validators=[
        validators.DataRequired(),
    ])
    surname = StringField('Surname', validators=[
        validators.DataRequired(),
    ])
    submit = SubmitField('Register')


class EditUserForm(FlaskForm):
    current_password = PasswordField('Current Password')
    password = PasswordField('New Password', validators=[
        validators.Optional(),
        password_check,
        validators.Length(min=8, max=40, message = "password between 8 and 40 characters"),
        validators.EqualTo('confirm', message = 'Passwords must match')
    ])
    confirm = PasswordField('Repeat New Password')    
    submit = SubmitField('Edit')


class SellerRegisterForm(FlaskForm):
    username = StringField('Username', validators=[
        DataRequired(),
        username_check
        ])
    password = PasswordField('Password', validators=[
        validators.DataRequired(),
        password_check,
        validators.Length(min=8, max=40, message = "password between 8 and 40 characters"),
        validators.EqualTo('confirm', message = 'Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')    
    name = StringField('Name', validators=[
        validators.DataRequired(),
    ])
    submit = SubmitField('Register')


# def price_validator(form, field):
#     price = form.price.data
#     try:
#         number_of_digits = len(str(price).split(".")[1])
#         if(number_of_digits > 2):
#             raise ValidationError('Price has to have a valid value (in format 120.50)')
#     except:
#         raise ValidationError('Price has to have a valid value (in format 120.50)')

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    submit = SubmitField('Sign In')

class AddItemForm(FlaskForm):
    price = IntegerField('Price', validators=[
        DataRequired(),
        ])
    in_stock = IntegerField('In Stock', validators=[
        validators.InputRequired()
    ])
    description = StringField('Description', validators=[
        validators.DataRequired(),
    ])

    product_name = StringField('Name', validators=[
        validators.DataRequired(),
    ])
    submit = SubmitField('Add Item')

class EditItemForm(FlaskForm):

    item_id = HiddenField()
    price = IntegerField('Price', validators=[validators.Optional()])
    in_stock = IntegerField('In Stock', validators=[validators.Optional()])
    description = StringField('Description', validators=[validators.Optional()])

    product_name = StringField('Name', validators=[validators.Optional()])
    submit = SubmitField('Edit Item')

    def set_hidden_value(self, value):
        self.item_id.data = value


class CategoryAdditionForm(FlaskForm):
    name = StringField('Category Name', validators=[
        DataRequired(),
    ])
    submit = SubmitField('Add Category')
