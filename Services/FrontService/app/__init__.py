from flask import Flask
import os

from werkzeug import test
app = Flask(__name__)

app.config.from_object('config')

UPLOAD_FOLDER = os.path.join('app', 'static', 'uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.logged_in_time = 5 * 60 
app.basket = dict()
app.users_logged_in = dict()


from app import views
