from functools import wraps

from flask.helpers import url_for
from flask import Response
import requests

from app import app
from flask import render_template, flash, redirect, request, session, abort

from config import AUTH_SERVICE_URL, PAYMENT_SERVICE_URL, SEARCH_SERVICE_URL, STORAGE_SERVICE_URL
from .forms import AddItemForm, CategoryAdditionForm, EditItemForm, EditUserForm, LoginForm, ProductSearchForm, ProductSearchFormSeller, RegisterForm, SellerRegisterForm
from datetime import date, datetime, timedelta, timezone

import os
from werkzeug.utils import secure_filename

import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry

import json
from time import time, sleep

graphs = {}
graphs['success'] = Counter(
    'front_successful_requests_total', 'Number of successful requests.')
graphs['error'] = Counter('front_error_requests_total',
                          'Number of error requests.')

graphs['c'] = Counter('front_request_operations_total',
                      'Total number of requests')
graphs['h'] = Histogram('front_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))

# decorator function to make sure user is logged in to access certain pages


def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function


def login_required(f):
    @wraps(f)
    def login_required_decorated_function(*args, **kwargs):
        if not (session.get("loggedIn", default=False) == True and
                datetime.utcnow().replace(tzinfo=timezone.utc) - session.get("lastSeen", default=datetime(year=1, month=1, day=1, tzinfo=timezone.utc)) < timedelta(minutes=app.logged_in_time)):
            return redirect('/login')
        return f(*args, **kwargs)
    return login_required_decorated_function



def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try: 
            if response.status_code == 200:
                graphs['success'].inc()
            else:
                graphs['error'].inc()
        except:
                graphs['success'].inc()
        return response
    return wrapper



@app.route('/tester', methods=['GET', 'POST'])
@time_request
@count_requests
@categorize_response
def tester():
    start_time = time()
    graphs['c'].inc()
    sleep(0.6)
    end_time = time()
    graphs['h'].observe(end_time - start_time)
    return Response("I am just a tester", status=200, content_type='text/html')


@app.route('/metrics')
def metrics():
    res = []
    for k, v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')

# login page


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
@time_request
@count_requests
@categorize_response
def login():
    # if logged in, redirect to home
    if session.get("loggedIn", default=False) == True and \
            datetime.utcnow().replace(tzinfo=timezone.utc) - session.get("lastSeen", default=datetime(year=1, month=1, day=1, tzinfo=timezone.utc)) < timedelta(minutes=app.logged_in_time):
        return redirect('/home')
    # Otherwise log in
    form = LoginForm()

    if form.validate_on_submit():
        # Check if the user exists
        username = form.username.data
        password = form.password.data

        url = f'{AUTH_SERVICE_URL}/login'

        data = {
            "username": username,
            "password": password
        }
        response = requests.post(url, json=data)

        # if user exists, log in
        if response.status_code == 200:
            session["userName"] = username
            session["loggedIn"] = True
            session["lastSeen"] = datetime.utcnow()

            return redirect('/home')
        else:  # otherwise render failed login
            return Response(render_template('failed_login.html', message=response.text), status=200, content_type='text/html')

    return Response(render_template('login.html', form=form), status=200, content_type='text/html')


@app.route('/register', methods=['POST', 'GET'])
@time_request
@count_requests
@categorize_response
def register():

    print("Requst:", request)
    print("RQF:", request.form)
    form = RegisterForm()
    print("FORM:", form)
    print("VALIDATE:", form.validate_on_submit())

    if form.validate_on_submit():
        url = f'{AUTH_SERVICE_URL}/register'

        data = {
            "username": form.username.data,
            "password": form.password.data,
            "firstname": form.firstname.data,
            "surname": form.surname.data
        }
        response = requests.post(url, json=data)

        if response.status_code == 200:
            return redirect('/login')
        else:
            return Response(render_template('register.html',
                                            title='Register',
                                            form=form,
                                            message=response.text),
                            status=200,
                            content_type='text/html')

    return Response(render_template('register.html',
                                    title='Register',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/logout', methods=['POST', 'GET'])
@time_request
@count_requests
@categorize_response
def logout():
    session["loggedIn"] = False
    session["userName"] = ""
    session["lastSeen"] = 123

    return redirect('/login')


@app.route('/home', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def home():
    return Response(render_template('home.html', username=session['userName']),
                    status=200,
                    content_type='text/html')


@app.route('/all_items', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def all_items():
    url = f'{STORAGE_SERVICE_URL}/get_all_items'
    response = requests.get(url)
    try:
        items = response.json()['response'] if response.status_code != 200 else []
    except:
        items = []
    return Response(render_template('all_items.html', items=items),
                    status=200,
                    content_type='text/html')


@app.route('/delete_item', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def delete_item():
    url = f'{AUTH_SERVICE_URL}/get_my_id'
    data = {
        "username": session.get('userName')
    }
    response = requests.post(url, json=data)
    my_id = response.text

    url = f'{SEARCH_SERVICE_URL}/find_by_id/{request.form["item_id"]}'
    response = requests.get(url)
    if response.status_code != 200:
        return redirect("/home")
    if int(response.json()['selling_user_id']) != int(my_id):
        return redirect("/home")

    url = f"{STORAGE_SERVICE_URL}/{request.form['item_id']}"
    response = requests.delete(url)
    return redirect('/home')


@app.route('/delete_from_basket/<removed_id>', methods=['GET', 'POST'])
@app.route('/delete_from_basket', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def delete_from_basket(removed_id=None):
    if removed_id == None:
        if 'item_id' not in request.form.keys():
            abort(400, "Delete from basket had no item_id param")

        if request.form.get('item_id') in app.basket[session.get('userName')]:
            app.basket[session.get('userName')].remove(
                request.form.get('item_id'))

        return redirect('/basket')
    else:

        if removed_id in app.basket[session.get('userName')]:
            app.basket[session.get('userName')].remove(removed_id)

        return redirect('/basket')


# @app.route('/item/<id>', methods=['GET', 'POST'])
# @app.route('/item', methods=['POST'])
# @login_required
# def item(id = None):
#     if id == None:
#         if 'item_id' not in request.form.keys():
#             app.logger.error(f'Item route had no item_id param', extra = { 'file_id': 'error.log' })

#             abort(400, "Bad Request")

#         product = Product.query.get(request.form['item_id'])

#         return render_template(
#                 "item.html",
#                 user_type = session['user_type'],
#                 item = product
#             )
#     else:
#         product = Product.query.get(id)

#         return render_template(
#             "item.html",
#             user_type = session['user_type'],
#             item = product
#             )


@app.route('/add_to_basket', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def add_to_basket():
    print("request.form:", request.form)
    if 'item_id' not in request.form.keys():
        abort(400, "Bad Request")

    if session.get('userName', default=None) == None:
        abort(401, "Unauthorized")
    if session.get('userName') not in app.basket.keys():
        app.basket[session.get('userName')] = set()

    app.basket[session.get('userName')].add(request.form.get('item_id'))

    return redirect('/basket')


@app.route('/basket', methods=['GET'])
@login_required
@time_request
@count_requests
@categorize_response
def basket():

    if session.get('userName') not in app.basket.keys():
        app.basket[session.get('userName')] = set()

    basket_items = []
    try:
        for item in app.basket[session.get('userName')]:
            url = f'{STORAGE_SERVICE_URL}/{item}'
            response = requests.get(url)
            basket_items.append(response.json()['response'])
    except:
        basket_items = []
    return Response(render_template('basket.html', basket_items=basket_items),
                    status=200,
                    content_type='text/html')


# @app.route('/favourite_items', methods=['GET'])
# @login_required
# @user_type_required
# def favourite_items():
#     return render_template(
#         'favourite.html',
#         favourite_items=User.query.get(session['user_id']).favourites
#     )


# def allowed_file(filename):
#     return '.' in filename and \
#            filename.rsplit('.', 1)[1].lower() in {
#                'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}


@app.route('/edit_item', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def edit_item():
    form = EditItemForm()
    url = f'{AUTH_SERVICE_URL}/get_my_id'
    data = {
        "username": session.get('userName')
    }
    response = requests.post(url, json=data)
    my_id = response.text
    if form.validate_on_submit():

        data = {}
        if form.price.data and form.price.data != "":
            data['price'] = form.price.data
        if form.in_stock.data and form.in_stock.data != "":
            data['in_stock'] = form.in_stock.data
        if form.product_name.data and form.product_name.data != "":
            data['product_name'] = form.product_name.data
        if form.description.data and form.description.data != "":
            data['description'] = form.description.data

        url = f"{STORAGE_SERVICE_URL}/{form.item_id.data}"
        response = requests.put(url, json=data)
        if response.status_code == 200:
            return redirect('/home')
        else:
            return Response(render_template('edit_item.html',
                                            title='AddItem',
                                            form=form,
                                            message=response.text
                                            ),
                            status=200,
                            content_type='text/html')

    if 'item_id' in request.args.keys():
        form.set_hidden_value(request.args['item_id'])

        url = f'{SEARCH_SERVICE_URL}/find_by_id/{request.args["item_id"]}'
        response = requests.get(url)
        if response.status_code != 200:
            return redirect("/home")
        if int(response.json()['selling_user_id']) != int(my_id):
            return redirect("/home")

    return Response(render_template('edit_item.html',
                                    title='AddItem',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/add_item', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def add_item():

    form = AddItemForm()
    if form.validate_on_submit():
        url = f'{AUTH_SERVICE_URL}/get_my_id'
        data = {
            "username": session.get('userName')
        }
        response = requests.post(url, json=data)
        my_id = response.text

        data = {
            "price": form.price.data,
            "in_stock": form.in_stock.data,
            "product_name": form.product_name.data,
            "description": form.description.data,
            "selling_user_id": my_id
        }
        url = f"{STORAGE_SERVICE_URL}/"
        response = requests.post(url, json=data)
        if response.status_code == 200:
            return redirect('/home')
        else:
            return Response(render_template('add_item.html',
                                            title='AddItem',
                                            form=form,
                                            message=response.text),
                            status=200,
                            content_type='text/html')

    return Response(render_template('add_item.html',
                                    title='AddItem',
                                    form=form),
                    status=200,
                    content_type='text/html')


@app.route('/my_items', methods=['POST', 'GET'])
@login_required
@time_request
@count_requests
@categorize_response
def my_items():
    url = f'{AUTH_SERVICE_URL}/get_my_id'
    data = {
        "username": session.get('userName')
    }
    response = requests.post(url, json=data)
    my_id = response.text

    url = f'{SEARCH_SERVICE_URL}/find_by_seller/{my_id}'
    response = requests.get(url)
    items = response.json()

    return Response(render_template(
        'your_found_items.html',
        seller_items=items),
        status=200,
        content_type='text/html')


@app.route('/item_search', methods=['POST', 'GET'])
@login_required
@time_request
@count_requests
@categorize_response
def item_search():

    form = ProductSearchForm()
    if form.validate_on_submit():

        found_items = []
        unique_ids = set()

        if form.product_name.data != "":
            url = f'{SEARCH_SERVICE_URL}/find_by_name/{form.product_name.data}'
            response = requests.get(url)
            if response.status_code == 200:
                for item in response.json():
                    if item['product_id'] not in unique_ids:
                        unique_ids.add(item['product_id'])
                        found_items.append(item)

        if form.seller.data != "":
            url = f'{SEARCH_SERVICE_URL}/find_by_seller/{form.seller.data}'
            response = requests.get(url)
            if response.status_code == 200:
                for item in response.json():
                    if item['product_id'] not in unique_ids:
                        unique_ids.add(item['product_id'])
                        found_items.append(item)

        if form.price_min.data and form.price_min.data != "":
            found_items = list(
                filter(lambda x: x['price'] >= form.price_min.data, found_items))
        if form.price_min.data and form.price_max.data != "":
            found_items = list(
                filter(lambda x: x['price'] <= form.price_max.data, found_items))
        if form.price_min.data and form.in_stock.data != "":
            found_items = list(
                filter(lambda x: x['in_stock'] > 0, found_items))

        return Response(render_template(
            'found_items.html',
            found_items=list(found_items)),
            status=200,
            content_type='text/html')

    return Response(render_template('item_search.html',
                                    title='Search Items',
                                    form=form),
                    status=200,
                    content_type='text/html')

# @app.route('/add_category', methods=['POST', 'GET'])
# @login_required
# @seller_type_required
# def add_category():

#     form = CategoryAdditionForm()

#     if form.validate_on_submit():

#         if ProductCategory.query.filter_by(name=form.name.data).first() != None:
#             return redirect('/seller_home')

#         newCategory = ProductCategory(
#             name=form.name.data
#         )
#         db.session.add(newCategory)
#         db.session.commit()
#         app.logger.info(f'Seller Added a Category: {session["user_id"]} - {newCategory.category_id} ', extra = { 'file_id': 'info.log' })

#         return redirect('/seller_home')

#     return render_template('add_category.html',
#                            title='Add A Product Category',
#                            form=form)


@app.route('/user_settings', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def user_settings():

    message = ""

    form = EditUserForm()
    if form.validate_on_submit():
        url = f'{AUTH_SERVICE_URL}/edit'
        data = {
            "current_username": session.get('userName'),
            "current_password": form.current_password.data,
            "password": form.password.data
        }
        response = requests.post(url, json=data)
        if response.status_code == 200:
            message = "Success!"
        else:
            message = "Failed!"

    return Response(render_template('user_settings.html',
                                    form=form,
                                    message=message),
                    status=200,
                    content_type='text/html')


@app.route('/checkout', methods=['GET', 'POST'])
@login_required
@time_request
@count_requests
@categorize_response
def checkout():

    total = 0
    for product, value in request.form.items():
        if product[:8] != 'product_':
            continue
        product_id = product[8:]
        url = f'{STORAGE_SERVICE_URL}/{product_id}'
        response = requests.get(url)
        total += response.json()['response']['price'] * int(value)

    products = [[k, v] for k, v in request.form.items()]

    return Response(render_template('show_total.html',
                                    total=total,
                                    products=products),
                    status=200,
                    content_type='text/html')


@app.route('/pay', methods=['POST'])
@login_required
@time_request
@count_requests
@categorize_response
def pay():
    itemlist = json.loads(request.form.get('prods').replace("'", '"'))

    url = f'{PAYMENT_SERVICE_URL}/pay'
    data = {
        "card_number": request.form.get("card_number"),
        "cvv": request.form.get("cvv"),
        "expiry_date": request.form.get("expiry_date")
    }
    response = requests.post(url, json=data)

    if response.status_code == 200:
        app.basket[session.get('userName')] = set()
        return Response(render_template('paymentSuccess.html'),
                        status=200,
                        content_type='text/html')
    else:
        return Response(render_template('paymentFailed.html'),
                        status=200,
                        content_type='text/html')
