from flask import Flask
from flask.helpers import url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os

from flask_restful import Api
from werkzeug import test
app = Flask(__name__)

app.config.from_object('config')

db = SQLAlchemy(app)
# handle migrations
migrate = Migrate(app, db)

from app.models import User
db.create_all()


from app import views
