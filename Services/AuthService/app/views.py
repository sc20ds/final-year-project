

from functools import wraps
from app import app
from app import db
from app.models import User
from flask import Response, request
from datetime import datetime

from functools import wraps
import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry
from time import time, sleep



graphs = {}
graphs['success'] = Counter(
    'auth_successful_requests_total', 'Number of successful requests.')
graphs['error'] = Counter('auth_error_requests_total',
                          'Number of error requests.')

graphs['c'] = Counter('auth_request_operations_total',
                      'Total number of requests')
graphs['h'] = Histogram('auth_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))


def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function


def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try: 
            if response.status_code == 200:
                graphs['success'].inc()
            else:
                graphs['error'].inc()
        except:
                graphs['success'].inc()
        return response
    return wrapper
    
    
@app.route('/metrics')
def metrics():
    res = []
    for k, v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')




@app.route('/register', methods=['POST'])
@time_request
@count_requests
@categorize_response
def register():

    data = request.get_json()

    try:
        username = data.get('username')
        password = data.get('password')
        firstname = data.get('firstname')
        surname = data.get('surname')
    except Exception as e:
        return "Bad Request", 400

    if username == "":
        return "'username' parameter missing", 400
    if password == "":
        return "'password' parameter missing", 400
    if firstname == "":
        return "'firstname' parameter missing", 400
    if surname == "":
        return "'surname' parameter missing", 400

    if User.query.filter_by(username = username).first():
        return 'Username already taken', 409

    new_user = User(
        username = username,
        password = password,
        firstname = firstname,
        surname = surname,
        updated_at = datetime.utcnow(),
        created_at = datetime.utcnow()
    )
    db.session.add(new_user)
    db.session.commit()

    return "Success", 200

@app.route('/login', methods=['POST'])
@time_request
@count_requests
@categorize_response
def login():

    data = request.get_json()

    try:
        username = data.get('username')
        password = data.get('password')
    except:
        return "Bad Request", 400
                    
    if username == "":
        return "'username' parameter missing", 400
    if password == "":
        return "'password' parameter missing", 400

    if User.query.filter_by(username = username, password = password).first():
        return "OK", 200

    return 'Unauthorized', 401

@app.route('/get_my_id', methods=['POST'])
@time_request
@count_requests
@categorize_response
def get_my_id():

    data = request.get_json()

    try:
        username = data.get('username')
    except:
        return "Bad Request", 400
                    
    if username == "":
        return "'username' parameter missing", 400

    user = User.query.filter_by(username = username).first()
    if not user:
        return 'Unauthorized', 401

    return str(user.user_id), 200 


@app.route('/edit', methods=['POST'])
@time_request
@count_requests
@categorize_response
def update_user():
    data = request.get_json()
    current_username = data.get('current_username')
    current_password = data.get('current_password')

    new_username = data.get('username')
    new_password = data.get('password')
    new_firstname = data.get('firstname')
    new_lastname = data.get('lastname')

    # Connect to the database and retrieve the user with the given username
    user = User.query.filter_by(username = current_username, password = current_password).first()
    if not user:
        return 'Unauthorized', 401

    # Update the username, password, firstname, and/or lastname if they were provided
    if new_username:
        user.username = new_username
    if new_password:
        user.password = new_password
    if new_firstname:
        user.firstname = new_firstname
    if new_lastname:
        user.lastname = new_lastname

    # Save the updated user to the database
    db.session.commit()
    return 'User updated', 200


