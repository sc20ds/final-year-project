from app import db

# A table for a CUSTOMER
class User (db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(250), index=True)
    password = db.Column(db.String(250), index=True)

    firstname = db.Column(db.String(250), index=True)
    surname = db.Column(db.String(250), index=True)

    updated_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime)

    def __repr__(self):
        return f'<User {self.username}: {self.firstname} {self.surname}>'
