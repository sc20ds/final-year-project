Flask-Babel==2.0.0
Flask-RESTful==0.3.9
python-dateutil==2.8.1
jinja2 < 3.1.0
itsdangerous==2.0.1
werkzeug==2.0.3
Flask==2.1.3
prometheus-client
requests