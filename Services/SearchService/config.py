import os

# Enabling CSRF protection
WTF_CSRF_ENABLED = True
SECRET_KEY = 'secret-key-1234-#$%^'

# setting prject's base directory
basedir = os.path.abspath(os.path.dirname(__file__))

STORAGE_SERVICE_URL = os.environ.get("STORAGE_SERVICE_URL")
