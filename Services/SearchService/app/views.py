
from flask import Response

from app import app


import prometheus_client
from prometheus_client import Counter, Histogram

from time import time

from config import STORAGE_SERVICE_URL


app.graphs['c'] = Counter('python_request_operations_total', 'Total number of requests')
my_buckets = [0, 1, 2, 5, 6, 10, 10000]
app.graphs['h'] = Histogram('python_request_duration_seconds', 'Histogram for request durations', buckets = my_buckets)


@app.route('/tester', methods = ['GET', 'POST'])
def tester():
    start_time = time()
    app.graphs['c'].inc()
    end_time = time()
    duration = end_time - start_time
    app.graphs['h'].observe(duration)

    return Response(STORAGE_SERVICE_URL)


@app.route('/metrics')
def metrics():
    res = []
    for k, v in app.graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')

