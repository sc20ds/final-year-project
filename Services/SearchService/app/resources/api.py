from flask.json import jsonify
from flask_restful import Resource

from flask import Response, request
from app import app
import json

from config import STORAGE_SERVICE_URL
import requests

from functools import wraps
import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry
from time import time, sleep

def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        app.graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        app.graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function


def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try: 
            if response.status_code == 200:
                app.graphs['success'].inc()
            else:
                app.graphs['error'].inc()
        except:
                app.graphs['success'].inc()
        return response
    return wrapper

    

class NameFilter(Resource):
    @time_request
    @count_requests
    @categorize_response
    def get(self, name):
        url = f'{STORAGE_SERVICE_URL}/get_all_items'

        response = requests.get(url)

        if response.status_code == 200:
            return [ product for product in response.json()['response'] if product['product_name'] == name], 200
        else:
            return f"Failed with code {response.status_code}", response.status_code


class IdFilter(Resource):
    @time_request
    @count_requests
    @categorize_response
    def get(self, id):
        url = f'{STORAGE_SERVICE_URL}/{id}'

        response = requests.get(url)
        if response.status_code == 200:
            return response.json()['response'], 200
        else:
            return f"Failed with code {response.status_code}", response.status_code

class SellingFilter(Resource):
    @time_request
    @count_requests
    @categorize_response
    def get(self, selling_user_id):
        url = f'{STORAGE_SERVICE_URL}/get_all_items'

        response = requests.get(url)
        if response.status_code == 200:
            return [ product for product in response.json()['response'] if product['selling_user_id'] == int(selling_user_id)], 200
        else:
            return f"Failed with code {response.status_code}", response.status_code

