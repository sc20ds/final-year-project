from flask import Flask
from flask.helpers import url_for
import os
from prometheus_client import Summary, Counter, Histogram, Gauge

from flask_restful import Api
app = Flask(__name__)
app.config.from_object('config')

app.graphs = {}
app.graphs['success'] = Counter(
    'search_successful_requests_total', 'Number of successful requests.')
app.graphs['error'] = Counter('search_error_requests_total',
                          'Number of error requests.')

app.graphs['c'] = Counter('search_request_operations_total',
                      'Total number of requests')
app.graphs['h'] = Histogram('search_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))

from app.resources.api import IdFilter, NameFilter, SellingFilter 

api = Api(app)
api.add_resource(NameFilter, '/find_by_name/<name>')
api.add_resource(IdFilter, '/find_by_id/<id>')
api.add_resource(SellingFilter, '/find_by_seller/<selling_user_id>')

from app import views
