from functools import wraps
import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry
from time import time, sleep

from app import app
from flask import Response, request

graphs = {}
graphs['success'] = Counter(
    'payment_successful_requests_total', 'Number of successful requests.')
graphs['error'] = Counter('payment_error_requests_total',
                          'Number of error requests.')

graphs['c'] = Counter('payment_request_operations_total',
                      'Total number of requests')
graphs['h'] = Histogram('payment_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))


def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function

def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try: 
            if response.status_code == 200:
                graphs['success'].inc()
            else:
                graphs['error'].inc()
        except:
                graphs['success'].inc()
        return response
    return wrapper
    
    
@app.route('/metrics')
def metrics():
    res = []
    for k, v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')


@app.route('/pay', methods=['POST'])
@time_request
@count_requests
@categorize_response
def pay():

    data = request.get_json()
    print(data)
    try:
        card_number = data.get('card_number')
        cvv = data.get('cvv')
        expiry_date = data.get('expiry_date')
    except:
        return "Bad Request", 400
                    
    if card_number == "":
        return "'card_number' parameter missing", 400
    if cvv == "":
        return "'cvv' parameter missing", 400
    if expiry_date == "":
        return "'expiry_date' parameter missing", 400

    print(card_number)
    if card_number == '4242' * 4:
        return "OK", 200
    else:
        return "Card Number or CVV Invalid", 402

