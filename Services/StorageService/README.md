# Run The application

1. Create Virtual Environment:
<code> python3 -m venv venv </code>
<code> source venv/bin/activate </code>

2. Install Dependencies 
<code> pip3 install -r requirements.txt </code>

3. Run the application:
<code> python3 run.py </code>

# Containerize the application

docker build . -t <application_tag>
docker run -p <chosen_port>:5000 <application_tag>

