from app import db
category_belonging = db.Table('category_belonging', db.Model.metadata,
                              db.Column('category_id', db.Integer,
                                        db.ForeignKey('product_category.category_id')),
                              db.Column('product_id', db.Integer,
                                        db.ForeignKey('product.product_id'))
                              )

class Product (db.Model):

    product_id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Float, index=True)
    in_stock = db.Column(db.Integer, index=True)
    product_name = db.Column(db.String(200))
    description = db.Column(db.Text)
    selling_user_id = db.Column(db.Integer)

    categories = db.relationship(
        'ProductCategory', secondary=category_belonging)

    def __repr__(self):
        return f"<Product {self.product_id}: {self.product_name}>"

    @property
    def serialize(self):
        return {
            'product_id': self.product_id,
            'price': self.price,
            'in_stock': self.in_stock,
            'product_name': self.product_name,
            'description': self.description,
            'selling_user_id': self.selling_user_id,
            'categories': self.serialize_categories
        }

    @property
    def serialize_categories(self):
        return [item.serialize for item in self.categories]


class ProductCategory(db.Model):
    category_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)

    products = db.relationship(
        'Product', secondary=category_belonging, overlaps="categories")

    def __repr__(self):
        return f"<ProductCategory {self.category_id}: {self.name}>"

    # serializing same as described above
    @property
    def serialize(self):
        return {
            'category_id': self.category_id,
            'name': self.name
        }
