from flask import Response
from app import app

from functools import wraps
import prometheus_client
from prometheus_client import Summary, Counter, Histogram, Gauge
from prometheus_client.core import CollectorRegistry
from time import time, sleep

@app.route('/metrics')
def metrics():
    res = []
    for k, v in app.graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype='text/plain')
