from functools import wraps
import os
from flask.json import jsonify
from flask_restful import Resource

from flask import request
from werkzeug.utils import secure_filename
from app import app, db
from app.models import Product, ProductCategory
import json
from time import time, sleep


def time_request(f):
    @wraps(f)
    def time_request_decorated_function(*args, **kwargs):
        start_time = time()
        result = f(*args, **kwargs)
        end_time = time()
        app.graphs['h'].observe(end_time - start_time)
        return result
    return time_request_decorated_function


def count_requests(f):
    @wraps(f)
    def count_requests_decorated_function(*args, **kwargs):
        app.graphs['c'].inc()
        return f(*args, **kwargs)
    return count_requests_decorated_function

def categorize_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try: 
            if response.status_code == 200:
                app.graphs['success'].inc()
            else:
                app.graphs['error'].inc()
        except:
                app.graphs['success'].inc()
        return response
    return wrapper



class ItemListResource(Resource):    
    @time_request
    @count_requests
    @categorize_response
    def get(self):
        items_from_db = Product.query.all()

        return jsonify(
            response = [
                item.serialize for item in items_from_db
            ]
        )
 
class Item(Resource):    
    @time_request
    @count_requests
    @categorize_response
    def get(self, item_id):
        item_from_db = Product.query.get(item_id)
        if item_from_db == None:
            return "Item not found", 404
        return jsonify(
            response = item_from_db.serialize
        )
    
    @time_request
    @count_requests
    @categorize_response
    def put(self, item_id):
        data = request.get_json()
        product = Product.query.get(item_id)
        if product == None:
            response = {'message' : "Product with this ID doesn't exist"}
            return response, 404


        if data.get('username') != None:
            product.price = data['price']

        if data.get('in_stock') != None:
            if isinstance(data.get('in_stock'), int): 
                product.in_stock = data['in_stock']
            else:
                return "In stock has to be a non-negative integer", 400


        if data.get('product_name') != None and isinstance(data.get('product_name'), str):
            product.product_name = data['product_name']

        if data.get('description') != None:
            product.description = data['description']

        db.session.commit()

        if 'categories' in data:
            product.categories = []
            for cat in json.loads(data['categories']):
                    category = ProductCategory.query.get(cat)
                    if category != None:
                        product.categories.append(category)

        db.session.commit()
        return product.serialize, 200

    @time_request
    @count_requests
    @categorize_response
    def delete(self, item_id):
        product = Product.query.get(item_id)
        if product == None:
            response = {'message' : "Product with this ID doesn't exist"}
            return response, 401

        Product.query.filter_by(product_id=item_id).delete()
        db.session.commit()
        return "Removed", 200
        

class AddItem(Resource):
    @time_request
    @count_requests
    @categorize_response
    def post(self):
        data = request.get_json()

        if data.get('price') == None:
            response = {'message' : "'price' field doesn't exist or the value is invalid"}
            return response, 400
        if data.get('in_stock') == None or not isinstance(data['in_stock'], int): 
            response = {'message' : "'in_stock' field doesn't exist or the value is invalid (integer)"}
            return response, 400
        if data.get('product_name') == None:
            response = {'message' : "'product_name' field doesn't exist"}
            return response, 400
            
        if data.get('description') == None:
            response = {'message' : "'description' field doesn't exist"}
            return response, 400

        if data.get('selling_user_id') == None:
            response = {'message' : "'selling_user_id' field doesn't exist"}
            return response, 400

        new_product = Product(
            price = data.get('price'),
            in_stock = data.get('in_stock'),
            product_name = data.get('product_name'),
            description = data.get('description'),
            selling_user_id = data.get('selling_user_id'),
        )

        db.session.add(new_product)
        db.session.commit()

        if 'categories' in data:
            for cat in json.loads(data.get('categories')):
                    category = ProductCategory.query.get(cat)
                    if category != None:
                        new_product.categories.append(category)
        db.session.commit()
        return new_product.serialize, 200