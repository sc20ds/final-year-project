from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os
from prometheus_client import Summary, Counter, Histogram, Gauge

from flask_restful import Api
from werkzeug import test
app = Flask(__name__)

app.config.from_object('config')
app.graphs = {}

db = SQLAlchemy(app)
migrate = Migrate(app, db)
app.graphs['success'] = Counter(
    'storage_successful_requests_total', 'Number of successful requests.')
app.graphs['error'] = Counter('storage_error_requests_total',
                          'Number of error requests.')

app.graphs['c'] = Counter('storage_request_operations_total',
                      'Total number of requests')
app.graphs['h'] = Histogram('storage_request_duration_seconds',
                        'Histogram for request durations', buckets=(1, 2, 5, 6, 10, 10000))

from app.models import Product
db.create_all()

from app.resources.api import  ItemListResource, Item, AddItem

api = Api(app)
api.add_resource(ItemListResource, '/get_all_items')
api.add_resource(Item, '/<int:item_id>')

api.add_resource(AddItem, '/')

from app import views