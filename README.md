# Final Year Project



# JMeter 
The JMeter application available in JMeter/ folder. A README with the instructions to run it are available inside the directory.


# Monolith
The monolithic application alongside the instructions on how to run it is available in Monolith/ directory.
  
# Microservice
The microservice application alongside the instructions on how to run it is available in Services/ directory.


# Kubernetes
The Kubernetes Deployment is available in the Kubernetes/ directory. 

The script deploy.sh can be used to start the application

The script destroy.sh can be used to delete the application

